package com.example.wificonnectordemo.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.example.wificonnectordemo.R

abstract class BaseAlertDialog: DialogFragment() {

    var btnClick: () -> Unit = {}

    var btnCloseClick: () -> Unit = {}

    @LayoutRes
    open fun getLayoutRes(): Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        setStyle(STYLE_NORMAL, R.style.DialogStyle)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setWindowAnimations(R.style.dialog_animation_fade)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return if (getLayoutRes() == -1) super.onCreateView(inflater, container, savedInstanceState)
        else inflater.inflate(getLayoutRes(), container, false)
    }

    fun setBtnListener(listener: () -> Unit): BaseAlertDialog {
        btnClick = listener
        return this
    }

    open fun setBtnCloseListener(listener: () -> Unit): BaseAlertDialog {
        btnCloseClick = listener
        return this
    }

    fun show(manager: FragmentManager?) {
        manager?.let { show(manager, BaseAlertDialog::class.java.simpleName) }
    }
}
