package com.example.wificonnectordemo.dialog

import android.os.Bundle
import android.view.View
import com.example.wificonnectordemo.R
import kotlinx.android.synthetic.main.dialog_layout_connect_to_network.*

class ConnectToNetworkAlertDialog: BaseAlertDialog() {

    companion object {
        fun newInstance() = ConnectToNetworkAlertDialog()
    }

    override fun getLayoutRes() =
        R.layout.dialog_layout_connect_to_network

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_connect.setOnClickListener { btnClick.invoke() }
        dialog_close.setOnClickListener { btnCloseClick.invoke() }
    }

    fun getTextFromEditText() = et_password.text.toString().trim()

    fun showPasswordError() {
        text_input_layout.error = getString(R.string.message_empty_password)
    }
}

