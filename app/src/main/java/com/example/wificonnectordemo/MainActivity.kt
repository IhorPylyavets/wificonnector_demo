package com.example.wificonnectordemo

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.wifi.ScanResult
import android.net.wifi.WifiManager
import android.net.wifi.WifiNetworkSpecifier
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wificonnectordemo.dialog.ConnectToNetworkAlertDialog
import com.squareup.picasso.Callback
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {

    companion object {
        private const val MY_REQUEST_CODE = 123
        private const val IMAGE_URL = "https://image.shutterstock.com/image-photo/mountains-during-sunset-beautiful-natural-260nw-407021107.jpg"
    }

    private var wifiManager: WifiManager? = null
    private var wifiReceiver: WifiBroadcastReceiver? = null

    private lateinit var adapter: WiFiListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        wifiManager = this.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wifiReceiver = WifiBroadcastReceiver()
        registerReceiver(wifiReceiver, IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))

        checkWifiState()

        button_scan.setOnClickListener { askAndStartScanWifi() }

        recycler_view.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }

    private var selectedWiFiItem: WiFiItem? = null

    private fun setDataToAdapter(data: List<WiFiItem>) {
        text_list_title.visibility = View.VISIBLE

        adapter = WiFiListAdapter(data)
        recycler_view.adapter = adapter
        adapter.setOnItemClickListener { _, item, _, _ ->
            selectedWiFiItem = item
            connectToNetworkAlertDialog.show(this.supportFragmentManager)
        }
    }

    private val connectToNetworkAlertDialog: ConnectToNetworkAlertDialog by lazy {
        ConnectToNetworkAlertDialog.newInstance().apply {
            setBtnListener {
                activity?.let {
                    val password = getTextFromEditText().trim()

                    if (password.isEmpty()) {
                        showPasswordError()
                    } else {
                        dismiss()
                        selectedWiFiItem?.let { wifiItem ->
                            connectToNetwork(wifiItem, password)
                        }
                    }
                }
            }
            setBtnCloseListener {
                activity?.finish()
            }
        }
    }

    override fun onStop() {
        unregisterReceiver(wifiReceiver)
        super.onStop()
    }

    private fun checkWifiState() {
        wifiManager?.let {
            val state = it.wifiState
            var statusInfo = "Unknown"
            statusInfo = when (state) {
                WifiManager.WIFI_STATE_DISABLING -> "Disabling"
                WifiManager.WIFI_STATE_DISABLED -> "Disabled"
                WifiManager.WIFI_STATE_ENABLING -> "Enabling"
                WifiManager.WIFI_STATE_ENABLED -> "Enabled"
                WifiManager.WIFI_STATE_UNKNOWN -> "Unknown"
                else -> "Unknown"
            }
            showToast("Wifi Status: $statusInfo")

            if (state == WifiManager.WIFI_STATE_ENABLED) {
                button_scan.isClickable = true
            }
        }
    }

    private fun askAndStartScanWifi() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permission1 = ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )

            if (permission1 != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                    this, arrayOf(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_WIFI_STATE,
                        Manifest.permission.ACCESS_NETWORK_STATE
                    ), MY_REQUEST_CODE
                )
                return
            }
        }
        this.doStartScanWifi()
    }

    private fun doStartScanWifi() {
        wifiManager?.let {
            progress_bar.visibility = View.VISIBLE
            it.startScan()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            MY_REQUEST_CODE -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    doStartScanWifi()
                }
            }
        }
    }

    private fun connectToNetwork(
        wiFiItem: WiFiItem,
        password: String
    ) {
        val builder = WifiNetworkSpecifier.Builder()
        builder.setSsid(wiFiItem.networkSSID)
        builder.setWpa2Passphrase(password)

        val wifiNetworkSpecifier = builder.build()

        val networkRequestBuilder = NetworkRequest.Builder().apply {
            addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            setNetworkSpecifier(wifiNetworkSpecifier)
        }

        val networkRequest = networkRequestBuilder.build()
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkCallback = object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network);
                cm.bindProcessToNetwork(network)
                showToast(getString(R.string.text_internet_available))

                thread {
                    Handler(this@MainActivity.mainLooper).postDelayed({
                        showToast(getString(R.string.text_start_image_loading))
                        progress_bar.visibility = View.VISIBLE

                        Picasso.get()
                            .load(IMAGE_URL)
                            .error(R.drawable.ic_launcher_foreground)
                            .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                            .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                            .into(iv, object : Callback {
                                override fun onSuccess() {
                                    progress_bar.visibility = View.GONE
                                    showToast(getString(R.string.text_finish_image_loading))
                                }

                                override fun onError(e: Exception) {
                                    progress_bar.visibility = View.GONE
                                    showToast(getString(R.string.text_error_image_loading))
                                }
                            })
                    }, 4000)
                }
            }
        }
        cm.requestNetwork(networkRequest, networkCallback)
    }

    private fun showToast(message: String) {
        Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
    }

    inner class WifiBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val ok = intent.getBooleanExtra(WifiManager.EXTRA_RESULTS_UPDATED, false)
            progress_bar.visibility = View.GONE
            if (ok) {
                wifiManager?.let {
                    val results: List<ScanResult> = it.getScanResults()
                    val items = mutableListOf<WiFiItem>()
                    for (result in results) {
                        val networkCapabilities = result.capabilities
                        val networkSSID = result.SSID
                        items.add(WiFiItem(networkSSID, networkCapabilities))
                    }
                    setDataToAdapter(items)
                }
            }
        }
    }
}
