package com.example.wificonnectordemo

data class WiFiItem(
    val networkSSID: String,
    val capabilities: String
)
