package com.example.wificonnectordemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class WiFiListAdapter(private val list: List<WiFiItem>) : RecyclerView.Adapter<WiFiItemViewHolder>() {

    private var onItemClickListener: ((view: View, item: WiFiItem, position: Int, previousSelectedPosition: Int) -> Unit)? =
        null

    private var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WiFiItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return WiFiItemViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: WiFiItemViewHolder, position: Int) {
        val item: WiFiItem = list[position]
        holder.bind(item)
        holder.itemView.setOnClickListener {
            onItemClickListener?.invoke(it, item, position, selectedPosition)
            selectedPosition = position
        }
    }

    override fun getItemCount(): Int = list.size

    fun setOnItemClickListener(onItemClickListener: ((view: View, item: WiFiItem, position: Int, previousSelectedPosition: Int) -> Unit)?) {
        this.onItemClickListener = onItemClickListener
    }

}

class WiFiItemViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item, parent, false)) {
    private var tvSSID: TextView? = null
    private var tvCapabilities: TextView? = null

    init {
        tvSSID = itemView.findViewById(R.id.tv_ssid)
        tvCapabilities = itemView.findViewById(R.id.tv_capabilities)
    }

    fun bind(item: WiFiItem) {
        tvSSID?.text = item.networkSSID
        tvCapabilities?.text = item.capabilities
    }

}
